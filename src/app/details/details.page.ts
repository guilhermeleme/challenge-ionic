import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Http, Response } from '@angular/http'

import { LoadingController } from '@ionic/angular'

import { map } from 'rxjs/operators'

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss']
})
export class DetailsPage implements OnInit {
  bookId = ''
  buy = false
  like = false
  public book = {
    volumeInfo: {
      averageRating: 0,
      pageCount: '',
      title: '',
      publisher: '',
      description: '',
      imageLinks: {
        thumbnail: ''
      }
    },
    saleInfo: {
      saleability: '',
      buyLink: '',
      listPrice: {
        amount: 0
      }
    }
  }

  constructor(
    private activeRoute: ActivatedRoute,
    private http: Http,
    private router: Router,
    public loadingController: LoadingController
  ) {}

  ngOnInit() {
    this.activeRoute.params.subscribe(params => {
      if (params['id']) {
        this.bookId = params['id']
      }
    })

    return this.http
      .get(`https://www.googleapis.com/books/v1/volumes/${this.bookId}`)
      .pipe(map((res: Response) => res.json()))
      .subscribe(data => {
        this.book = data
        // console.log(this.book)
      })
  }

  goBack() {
    this.router.navigate(['/home'])
  }

  onRateChange(event) {}

  handleBuy() {
    this.buy = true
  }

  handleLike() {
    this.like = true
  }
}
