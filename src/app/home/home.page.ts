import { Component } from '@angular/core'
import { Http, Response } from '@angular/http'
import { map } from 'rxjs/operators'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  books = []
  offset = 0

  constructor(private http: Http) {
    this.getBooks(event)
  }

  getBooks(event) {
    return this.http
      .get(
        `https://www.googleapis.com/books/v1/volumes?q=designer&maxResults=20&startIndex=${
          this.offset
        }`
      )
      .pipe(map((res: Response) => res.json()))
      .subscribe(data => {
        this.offset += 20
        this.books = [...this.books, ...data.items]
        // console.log(this.books)
        event.target.complete()
      })
  }

  refreshBooks(event) {
    this.books = []
    this.offset = 0

    this.getBooks(event)
  }
}
