import { Component, OnInit } from '@angular/core'
import { Http, Response } from '@angular/http'
import { map } from 'rxjs/operators'

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss']
})
export class SearchPage implements OnInit {
  searchQuery = ''
  books = []
  offset = 0
  timeOut = null

  constructor(private http: Http) {}

  ngOnInit() {}

  getSearch() {
    clearTimeout(this.timeOut)
    this.timeOut = setTimeout(() => {
      this.books = []
      this.getBooks()
    }, 1000)
  }

  getBooks() {
    if (!this.searchQuery) return
    return this.http
      .get(
        `https://www.googleapis.com/books/v1/volumes?q=${
          this.searchQuery
        }&maxResults=20&startIndex=${this.offset}`
      )
      .pipe(map((res: Response) => res.json()))
      .subscribe(data => {
        this.books = data.items

        console.log(this.books)
      })
  }

  getMoreBooks(event) {
    if (!this.searchQuery) return
    return this.http
      .get(
        `https://www.googleapis.com/books/v1/volumes?q=${
          this.searchQuery
        }&maxResults=20&startIndex=${this.offset}`
      )
      .pipe(map((res: Response) => res.json()))
      .subscribe(data => {
        this.offset += 20
        this.books = [...this.books, ...data.items]
        event.target.complete()
        console.log(this.books)
      })
  }

  getClear() {
    this.books = []
    this.offset = 0
    // console.log(this.books)
    // console.log(this.offset)
  }
}
